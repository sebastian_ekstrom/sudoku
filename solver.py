import curses
import ui
from assets import get_problem, print_problem
from sudoku_tests import ex

@ex(1)
def value_in_row(grid, value, row):
    for col in range(9):
        if grid[row][col] == value:
            return True
    return False

@ex(2)
def value_in_column(grid, value, col):
    for row in range(9):
        if grid[row][col] == value:
            return True
    return False

@ex(3)
def get_box_coords(row, col):
    return row//3, col//3

@ex(4)
def value_in_box(grid, value, box_row, box_col):
    for row in range(3*box_row, 3*box_row + 3):
        for col in range(3*box_col, 3*box_col + 3):
            if grid[row][col] == value:
                return True
    return False

@ex(5)
def is_value_possible(grid, value, row, col):
    if grid[row][col] != 0:
        return False
    if value_in_row(grid, value, row):
        return False
    if value_in_column(grid, value, col):
        return False
    box_row, box_col = get_box_coords(row, col)
    if value_in_box(grid, value, box_row, box_col):
        return False
    return True

@ex(6)
def find_possible_values(grid, row, col):
    possible = []
    for value in range(1, 10):
        if is_value_possible(grid, value, row, col):
            possible.append(value)
    return possible

@ex(7)
def only_possible_value(grid, row, col):
    possible = find_possible_values(grid, row, col)
    if len(possible) == 1:
        return possible[0]
    return None

@ex(8)
def is_solved(grid):
    for row in range(9):
        for col in range(9):
            if grid[row][col] == 0:
                return False
    return True

@ex(9)
def only_valid_pos_in_row(grid, value, row, col):
    for col2 in range(9):
        if col != col2:
            if is_value_possible(grid, value, row, col2):
                return False
    return True

@ex(10)
def only_valid_pos_in_column(grid, value, row, col):
    for row2 in range(9):
        if row != row2:
            if is_value_possible(grid, value, row2, col):
                return False
    return True

@ex(11)
def only_valid_pos_in_box(grid, value, row, col):
    box_row, box_col = get_box_coords(row, col)
    for row2 in range(3*box_row, 3*box_row + 3):
        for col2 in range(3*box_col, 3*box_col + 3):
            if row != row2 or col != col2:
                if is_value_possible(grid, value, row2, col2):
                    return False
    return True
    
@ex(12)
def only_valid_pos(grid, row, col):
    possible = find_possible_values(grid, row, col)
    for value in possible:
        if only_valid_pos_in_row(grid, value, row, col):
            return value
        if only_valid_pos_in_column(grid, value, row, col):
            return value
        if only_valid_pos_in_box(grid, value, row, col):
            return value
    return None
    
def create_candidate_matrix(grid):
    possible = get_empty_candidate_matrix()
    for row in range(9):
        for col in range(9):
            possible[row][col] = find_possible_values(grid, row, col)
    return possible
    
def main(stdscreen):
    
    terminal_ui = ui.SudokuUI(stdscreen)
    
    grid = get_problem(4)
    #print_problem(grid)
    #input()
    
    terminal_ui.print_problem(grid, ui.RED)
    terminal_ui.wait_for_input()

    while(True):
        changed = False
        for row in range(9):
            for col in range(9):
                if is_solved(grid):
                    pass
                    quit()
                if grid[row][col] == 0:
                    value = only_possible_value(grid, row, col)
                    if value != None:
                        grid[row][col] = value
                        #print_problem(grid)
                        #input()
                        terminal_ui.update_problem(row, col, value, ui.BLUE)
                        terminal_ui.wait_for_input()
                        changed = True
                        continue
                    value = only_valid_pos(grid, row, col)
                    if value != None:
                        grid[row][col] = value
                        #print_problem(grid)
                        #input()
                        terminal_ui.update_problem(row, col, value, ui.BLUE)
                        terminal_ui.wait_for_input()
                        changed = True
        if not changed:
            print("Unable to solve problem.")
            quit()

curses.wrapper(main)
