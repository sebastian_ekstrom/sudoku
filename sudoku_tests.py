from grader import Assignment, Exercise, ExerciseBuilder, ExerciseDict, Test, TestBuilder, HintTest
from assets import get_problem

def ref_value_in_row(grid, value, row):
    for col in range(9):
        if grid[row][col] == value:
            return True
    return False
    
def ref_value_in_column(grid, value, col):
    for row in range(9):
        if grid[row][col] == value:
            return True
    return False
    
def ref_get_box_coords(row, col):
    return row//3, col//3
    
def ref_value_in_box(grid, value, box_row, box_col):
    for row in range(3*box_row, 3*box_row + 3):
        for col in range(3*box_col, 3*box_col + 3):
            if grid[row][col] == value:
                return True
    return False            

def ref_is_value_possible(grid, value, row, col):
    if grid[row][col] != 0:
        return False
    if ref_value_in_row(grid, value, row):
        return False
    if ref_value_in_column(grid, value, col):
        return False
    box_row, box_col = ref_get_box_coords(row, col)
    if ref_value_in_box(grid, value, box_row, box_col):
        return False
    return True
    
def ref_find_possible_values(grid, row, col):
    possible = []
    for value in range(1, 10):
        if ref_is_value_possible(grid, value, row, col):
            possible.append(value)
    return possible
    
def ref_only_possible(grid, row, col):
    possible = ref_find_possible_values(grid, row, col)
    if len(possible) == 1:
        return possible[0]
    return None
    
def ref_is_solved(grid):
    for row in range(9):
        for col in range(9):
            if grid[row][col] == 0:
                return False
    return True
    
def ref_only_valid_pos_in_row(grid, value, row, col):
    for col2 in range(9):
        if col != col2:
            if ref_is_value_possible(grid, value, row, col2):
                return False
    return True
    
def ref_only_valid_pos_in_column(grid, value, row, col):
    for row2 in range(9):
        if row != row2:
            if ref_is_value_possible(grid, value, row2, col):
                return False
    return True
    
def ref_only_valid_pos_in_box(grid, value, row, col):
    box_row, box_col = ref_get_box_coords(row, col)
    for row2 in range(3*box_row, 3*box_row + 3):
        for col2 in range(3*box_col, 3*box_col + 3):
            if row != row2 or col != col2:
                if ref_is_value_possible(grid, value, row2, col2):
                    return False
    return True
    
def ref_only_valid_pos(grid, row, col):
    possible = ref_find_possible_values(grid, row, col)
    for value in possible:
        if ref_only_valid_pos_in_row(grid, value, row, col):
            return value
        if ref_only_valid_pos_in_column(grid, value, row, col):
            return value
        if ref_only_valid_pos_in_box(grid, value, row, col):
            return value
    return None
    
    
exercises = ExerciseDict()

grid0 = get_problem(0)
grid1 = get_problem(1)
grid2 = get_problem(2)
grid3 = get_problem(3)

solved = [[3, 2, 6, 8, 9, 4, 7, 5, 1],
          [7, 4, 9, 2, 5, 1, 3, 8, 6],
          [1, 5, 8, 3, 6, 7, 9, 4, 2],
          [4, 9, 3, 7, 2, 6, 8, 1, 5],
          [6, 7, 1, 4, 8, 5, 2, 9, 3],
          [2, 8, 5, 1, 3, 9, 4, 6, 7],
          [9, 1, 2, 6, 7, 8, 5, 3, 4],
          [5, 3, 4, 9, 1, 2, 6, 7, 8],
          [8, 6, 7, 5, 4, 3, 1, 2, 9]]

test1 = TestBuilder("Problem 0: a 5 in row 0", True).with_args(grid0, 5, 0).create()
test2 = TestBuilder("Problem 0: no 4 in row 4", False).with_args(grid0, 4, 4).create()
ex1 = ExerciseBuilder("Test if a value exists in a row", ref_value_in_row).with_tests(test1, test2).create()
exercises.add(ex1, 1)

test1 = TestBuilder("Problem 1: a 1 in col 8", True).with_args(grid1, 1, 8).create()
test2 = TestBuilder("Problem 1: no 1 in col 4", False).with_args(grid1, 1, 4).create()
ex2 = ExerciseBuilder("Test if a value exists in a column", ref_value_in_column).with_tests(test1, test2).create()
exercises.add(ex2, 2)

test1 = TestBuilder("Cell 2,6 is in box 0,2", (0,2)).with_args(2,6).create()
test2 = TestBuilder("Cell 8,0 is in box 2,0", (2,0)).with_args(8,0).create()
ex3 = ExerciseBuilder("Find the box a cell is in", ref_get_box_coords).with_tests(test1, test2).with_hint("Use // for integer division").create()
exercises.add(ex3, 3)

test1 = TestBuilder("Problem 2: a 1 in box 0,2", True).with_args(grid2, 1, 0, 2).create()
test2 = TestBuilder("Problem 2: no 8 in box 1,0", False).with_args(grid2, 8, 1, 0).create()
ex4 = ExerciseBuilder("Test if a value exists in a box", ref_value_in_box).with_tests(test1, test2).create()
exercises.add(ex4, 4)

test1 = TestBuilder("Problem 3: a 2 is possible in cell 0,1", True).with_args(grid3, 2, 0, 1).create()
test2 = TestBuilder("Problem 3: a 2 is not possible in cell 3,4", False).with_args(grid3, 2, 3, 4).create()
hint1 = HintTest("You can't place a value in a cell that is already occupied", args = [True])
test3 = TestBuilder("Problem 3: a 2 is not possible in cell 0,0", False).with_args(grid3, 2, 0, 0).with_hint_tests(hint1).create()
ex5 = ExerciseBuilder("Test if it is possible to place a certain value in a cell", ref_is_value_possible).with_tests(test1, test2, test3).create()
exercises.add(ex5, 5)

test1 = TestBuilder("Problem 0: 2, 7 possible in cell 0,2", [2,7]).with_args(grid0, 0, 2).create()
test2 = TestBuilder("Problem 0: 1, 2, 4 possible in cell 8,6", [1,2,4]).with_args(grid0, 8, 6).create()
hint1 = HintTest("No values can be placed in a cell that is already occupied", args = [3,7,8])
test3 = TestBuilder("Problem 0: no values possible in cell 3,3", []).with_args(grid0, 3, 3).with_hint_tests(hint1).create()
ex6 = ExerciseBuilder("Find possible values that can be placed in a cell", ref_find_possible_values).with_tests(test1, test2, test3).create()
exercises.add(ex6, 6)

test1 = TestBuilder("Problem 1: 2 is the only possible value in cell 1,8", 2).with_args(grid1, 1, 8).create()
test2 = TestBuilder("Problem 1: no single possible value in cell 1,1", None).with_args(grid1, 1, 1).create()
ex7 = ExerciseBuilder("Find the single possible value that can be placed in a cell", ref_only_possible).with_tests(test1, test2).create()
exercises.add(ex7, 7)

test1 = TestBuilder("Detect complete problem", True).with_args(solved).create()
test2 = TestBuilder("Detect incomplete problem", False).with_args(grid3).create()
ex8 = ExerciseBuilder("Detect whether or not a problem grid has been entirely filled in", ref_is_solved).with_tests(test1, test2).create()
exercises.add(ex8, 8)

test1 = TestBuilder("Problem 0: cell 3,6 only possible place in row 3 for a 9", True).with_args(grid0, 9, 3, 6).create()
test2 = TestBuilder("Problem 1: cell 4,4 not only possible place in row 4 for a 1", False).with_args(grid1, 1, 4, 4).create()
ex9 = ExerciseBuilder("Determine if a given cell is the only possible place in a row for a given value", ref_only_valid_pos_in_row).with_tests(test1, test2).create()
exercises.add(ex9, 9)

test1 = TestBuilder("Problem 2: cell 1,5 only possible place in column 5 for a 4", True).with_args(grid2, 4, 1, 5).create()
test2 = TestBuilder("Problem 3: cell 4,2 not only possible place in column 2 for a 1", False).with_args(grid3, 1, 4, 2).create()
ex10 = ExerciseBuilder("Determine if a given cell is the only possible place in a column for a given value", ref_only_valid_pos_in_column).with_tests(test1, test2).create()
exercises.add(ex10, 10)

test1 = TestBuilder("Problem 3: cell 4,7 only possible place in box 1,2 for a 9", True).with_args(grid3, 9, 4, 7).create()
test2 = TestBuilder("Problem 3: cell 1,1 not only possible place in box 0,0 for a 4", False).with_args(grid3, 4, 1, 1).create()
ex11 = ExerciseBuilder("Determine if a given cell is the only possible place in a box for a given value", ref_only_valid_pos_in_box).with_tests(test1, test2).create()
exercises.add(ex11, 11)

test1 = TestBuilder("Problem 0: cell 3,6 only possible place in row 3 for a 9", 9).with_args(grid0, 3, 6).create()
test2 = TestBuilder("Problem 2: cell 1,5 only possible place in column 5 for a 4", 4).with_args(grid2, 1, 5).create()
test3 = TestBuilder("Problem 3: cell 4,7 only possible place in box 1,2 for a 9", 9).with_args(grid3, 4, 7).create()
test4 = TestBuilder("Problem 1: cell 6,1 not only possible place for any value", None).with_args(grid1, 6, 1).create()
ex12 = ExerciseBuilder("Determine whether a given cell is the only possible place for any value", ref_only_valid_pos).with_tests(test1, test2, test3, test4).create()
exercises.add(ex12, 12)

ex = Assignment.create("sudoku", exercises.get_dict())
