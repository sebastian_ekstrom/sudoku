import curses
import ui
from assets import get_problem, get_empty_candidate_matrix
from sudoku_tests import ex

@ex(1)
def value_in_row(grid, value, row):
    for col in range(9):
        if grid[row][col] == value:
            return True
    return False

@ex(2)
def value_in_column(grid, value, col):
    for row in range(9):
        if grid[row][col] == value:
            return True
    return False

@ex(3)
def get_box_coords(row, col):
    return row//3, col//3

@ex(4)
def value_in_box(grid, value, box_row, box_col):
    for row in range(3*box_row, 3*box_row + 3):
        for col in range(3*box_col, 3*box_col + 3):
            if grid[row][col] == value:
                return True
    return False

@ex(5)
def is_value_possible(grid, value, row, col):
    if grid[row][col] != 0:
        return False
    if value_in_row(grid, value, row):
        return False
    if value_in_column(grid, value, col):
        return False
    box_row, box_col = get_box_coords(row, col)
    if value_in_box(grid, value, box_row, box_col):
        return False
    return True

@ex(6)
def find_candidates(grid, row, col):
    candidates = set()
    for value in range(1, 10):
        if is_value_possible(grid, value, row, col):
            candidates.add(value)
    return candidates

@ex(7)
def only_possible_value(candidates, row, col):
    possible = candidates[row][col]
    if len(possible) == 1:
        return list(possible)[0]

@ex(8)
def is_solved(grid):
    for row in range(9):
        for col in range(9):
            if grid[row][col] == 0:
                return False
    return True

@ex(9)
def only_valid_pos_in_row(candidates, value, row, col):
    for col2 in range(9):
        if col != col2:
            if value in candidates[row][col2]:
                return False
    return True

@ex(10)
def only_valid_pos_in_column(candidates, value, row, col):
    for row2 in range(9):
        if row != row2:
            if value in candidates[row2][col]:
                return False
    return True

@ex(11)
def only_valid_pos_in_box(candidates, value, row, col):
    box_row, box_col = get_box_coords(row, col)
    for row2 in range(3*box_row, 3*box_row + 3):
        for col2 in range(3*box_col, 3*box_col + 3):
            if row != row2 or col != col2:
                if value in candidates[row2][col2]:
                    return False
    return True
    
@ex(12)
def only_valid_pos(candidates, row, col):
    possible = candidates[row][col]
    for value in possible:
        if only_valid_pos_in_row(candidates, value, row, col):
            return value
        if only_valid_pos_in_column(candidates, value, row, col):
            return value
        if only_valid_pos_in_box(candidates, value, row, col):
            return value
    
    
def find_in_row(possible, target, row, col):
    for col2 in range(9):
        if col2 != col and possible[row][col2] == target:
            return col2
            
def find_in_column(possible, target, row, col):
    for row2 in range(9):
        if row2 != row and possible[row2][col] == target:
            return row2

def find_in_box(possible, target, row, col):
    box_row, box_col = get_box_coords(row, col)
    for row2 in range(3*box_row, 3*box_row + 3):
        for col2 in range(3*box_col, 3*box_col + 3):
            if (row != row2 or col != col2) and possible[row2][col2] == target:
                return row2, col2
    return None,None

def find_naked_pair(possible, row, col):
    pair = possible[row][col]
    if len(pair) != 2:
        return None, None, None, None
    col2 = find_in_row(possible, pair, row, col)
    if col2 != None:
        rule_out_from_row(possible, pair, row, [col, col2])
    row2 = find_in_column(possible, pair, row, col)
    if row2 != None:
        rule_out_from_column(possible, pair, col, [row, row2])
    row3, col3 = find_in_box(possible, pair, row, col)
    if row3 != None:
        rule_out_from_box(possible, pair, row, col, [(row, col), (row3, col3)])
    return row2, col2, row3, col3
    
    
def create_candidate_matrix(grid):
    candidates = get_empty_candidate_matrix()
    for row in range(9):
        for col in range(9):
            candidates[row][col] = find_candidates(grid, row, col)
    return candidates
    
def rule_out(candidates, values, row, col):
    candidates[row][col] -= values

def rule_out_from_row(candidates, values, row, skip = []):
    for col in range(9):
        if col not in skip:
            rule_out(candidates, values, row, col)

def rule_out_from_column(candidates, values, col, skip = []):
    for row in range(9):
        if row not in skip:
            rule_out(candidates, values, row, col)

def rule_out_from_box(candidates, values, row, col, skip = []):
    box_row, box_col = get_box_coords(row, col)
    for row2 in range(3*box_row, 3*box_row + 3):
        for col2 in range(3*box_col, 3*box_col + 3):
            if (row2, col2) not in skip:
                rule_out(candidates, values, row2, col2)
    
def update_candidates(candidates, value, row, col):
    rule_out_from_row(candidates, {value}, row)
    rule_out_from_column(candidates, {value}, col)
    rule_out_from_box(candidates, {value}, row, col)
    
            
def only_possible_value_wrapper(grid, candidates, row, col):
    value = only_possible_value(candidates, row, col)
    if value != None:
        grid[row][col] = value
        candidates[row][col].clear()
        update_candidates(candidates, value, row, col)
        return value, True, "{} only possible candidate at ({},{})".format(value, row, col)
    return None, False, None
            
def only_valid_pos_wrapper(grid, candidates, row, col):
    value = only_valid_pos(candidates, row, col)
    if value != None:
        grid[row][col] = value
        candidates[row][col].clear()
        update_candidates(candidates, value, row, col)
        return value, True, "({},{}) only valid pos for {}".format(row, col, value)
    return None, False, None
        
def find_naked_pair_wrapper(grid, possible, row, col):
    row2, col2, row3, col3 = find_naked_pair(possible, row, col)
    msg = ""
    if row2 != None:
        msg += "Naked pair found at ({},{}), ({},{})".format(row, col, row2, col)
    if col2 != None:
        msg += "Naked pair found at ({},{}), ({},{})".format(row, col, row, col2)
    if row3 != None and row3 != row2 and col3 != col2:
        msg += "Naked pair found at ({},{}), ({},{})".format(row, col, row3, col3)
    if msg != "":
        return None, True, msg
    return None, False, None
    
    
def main(stdscreen):
    methods = [only_possible_value_wrapper, only_valid_pos_wrapper, find_naked_pair_wrapper]
    
    terminal_ui = ui.SudokuUI(stdscreen, True, True)
    
    grid = get_problem(5)
    candidates = create_candidate_matrix(grid)
    
    #print_problem(grid)
    #input()
    
    terminal_ui.print_problem(grid, ui.RED)
    terminal_ui.print_candidates(grid, candidates)
    terminal_ui.wait_for_input()

    while(True):
        changed = False
        for method in methods:
            for row in range(9):
                for col in range(9):
                    if is_solved(grid):
                        terminal_ui.output("Problem solved successfully!")
                        terminal_ui.wait_for_input()
                        quit()
                    if grid[row][col] == 0:
                        value, candidates_changed, message = method(grid, candidates, row, col)
                        if candidates_changed:
                            terminal_ui.print_candidates(grid, candidates)
                            if value != None:
                                terminal_ui.update_problem(row, col, value, ui.BLUE)
                            if message != None:
                                terminal_ui.output(message)
                            terminal_ui.wait_for_input()
                            changed = True
            if changed:
                break
        if not changed:
            terminal_ui.output("Unable to solve problem.")
            terminal_ui.wait_for_input()
            quit()

curses.wrapper(main)
